<?php
  require_once 'config.php';
 ?>
 <!DOCTYPE html>
 <html lang="en" dir="ltr">
   <head>
     <?php echo $app->getAdminHTMLHeader('Home'); ?>
   </head>
   <body>
     <?php require_once 'components.php'; ?>
     <div class="row">
         <nav class="purple darken-3">
            <div class="nav-wrapper">
              <a href="../../../index.php" class="brand-logo"></a>
              <ul id="nav-mobile" class="right hide-on-med-and-down">
                <li> <a href="pages/login.php"> Login </a> </li>
                <li> <a href="pages/registration.php"> New user </a> </li>
              </ul>
            <div>
         </nav>
     </div>
     <div class="col s12">
       <a class="center" href="pages/login.php"> <h2> Login page </h2> </a>
       <a class="center" href="pages/registration.php"> <h2> Registration page </h2> </a>
     </div>
     <?php echo $app->getAdminFooter(); ?>
   </body>
 </html>
