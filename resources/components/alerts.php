<?php
/* *************** Alerts ******************************* */
  if(!empty($_SESSION["succ"])){
    echo "<script> $(document).ready(function(){ M.toast({html: '".$_SESSION["succ"]."', classes: 'blue darken-3 bold'}) });</script>";
    unset($_SESSION["succ"]);
  }

  if(!empty($_SESSION["error"])){
    echo "<script> $(document).ready(function(){ M.toast({html: '". $_SESSION["error"] ."', classes: 'red darken-2 bold'}) }); </script>";
    unset($_SESSION["error"]);
  }

  if(!empty($_SESSION["sys"])){
    echo "<script> $(document).ready(function(){ M.toast({html: '". $_SESSION["sys"] ."', classes: 'purple bold'}) }); </script>";
    unset($_SESSION["sys"]);
  }

 ?>
