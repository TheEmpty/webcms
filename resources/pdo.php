<?php
/* **************** PDO Script ****************** */
define("dbserver", "127.0.0.1"); //Adresa k databázovém serveru
define("dbuser", "root"); //Uživatelské jméno
define("dbpass", "mysql"); //Heslo uživatele
define("dbname", "WebCMS"); //Název databáze

$db = new PDO(
"mysql:host=" .dbserver. ";dbname=" .dbname,dbuser,dbpass,
  array(
    PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
    PDO::MYSQL_ATTR_INIT_COMMAND => "SET CHARACTER SET utf8"
    )
);

 ?>
