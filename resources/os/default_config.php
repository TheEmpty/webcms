<?php
  $default_config = array(
    'mail_auth' => true,
    'version' => '0.1a',
    'websiteTitle' => 'myWebsite',
    'mode' => 1,
    'registrations' => true,
    'theme' => 1,
    'lockdown' => false,
    'favicon' => '',
    'userGroups' => true,
    //'timespace' => 'Czech Republic',
    'defaultLevel' => 1,
    'levels' => array(
      'ID' => '1',
      'Name' => 'User',
      'Description' => 'Level of permission for normal user',
      'Level' => '1',
      'Prefix' => '',
    ),
    'level_settings' => array(
      'LID' => '1',
      'AdvancedAdmin' => false,
      'ListUsers' => false,
      'Editusers' => false,
      'ListArticles' => false,
      'EditArtciles' => false,
      'ListSettings' => false,
      'EditSettings' => false,
      'ListAPI' => false,
      'EditAPI' => false,
      'ListShop' => false,
      'EditShop' => false,
      'ListPlugins' => false,
      'EditPlugins' => false,
      'ListGroups' => false,
      'EditGroups' => false,
      'ListNotes' => false,
      'EditNotes' => false,
    ),
    'groups' => array(
      '' => '',
    ),
    'confCreated' => '2018-08-30 15:19:00',
    'language' => 'cs-cz',

  );

 ?>
