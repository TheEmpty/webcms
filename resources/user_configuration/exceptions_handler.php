<?php
function exceptions_handler($e){
  $eh = '
  <!DOCTYPE HTML>
    <html>
      <head>
         <title> Modul_cms | Kritická chyba aplikace </title>
         <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
         <!--Import materialize.css-->
         <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
         <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
         <!--Let browser know website is optimized for mobile-->
         <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
         <meta charset="utf-8">
      </head>
      <body>
        <div class="row center">
          <div class="col s8 offset-s2">
            <h1 style="margin-top: 10%; text-decoration: underline"> Modul_CMS </h1>
            <i class="material-icons large"> error cloud_off flash_off highlight_off not_interested error_outline </i>
            <h4>
              Omlouváme se, ale byla zjištěna kritická chyba! Prosíme, počkejte do restartu služby, nebo kontaktujte administrátora.
            </h4>
            <div class="row red center-align" style="margin-top: 10%">
              <p>'. $e->getMessage() .'</p>
            </div>
          </div>
        </div>
      </body>
    </html>
  ';

  echo $eh;
  error_log($e->getMessage());
}
?>
