<?php
  /*
                          ////////////////////////////////////////////////////////////////
                            ///////// ABSOLUTELY NEVER ALTER THIS FILE!! //////////////
                          ///////////////////////////////////////////////////////////////
  */
  //Starting session
  session_start();

  //Requiring important objects
  require_once "objects/app.php";
  require_once "objects/user.php";

  /* ******************** Custom Exception Handler ************************ */
  require_once "resources/user_configuration/exceptions_handler.php";
  set_exception_handler( "exceptions_handler" );

  //Requiring important resources
  require_once "resources/pdo.php";
  require_once "resources/os/app_config.php";
  require_once "resources/os/default_config.php";

  //Preventing to generate another instance of APP class with settings and already loaded configuration
  if(empty($_SESSION["system_configuration_class"])){
    $app = new APP($db, $app_config, $default_config);
    $_SESSION["system_configuration_class"] = serialize($app);
  }else{
    $app = unserialize($_SESSION["system_configuration_class"]);
    $app->fetchPDO($db);
  }

  /* **************** PHP Definitions *********************** */
  define("ActualDate", date("Y-m-d"));
  define("rootUrl", $app->show("url"));

  /* **************** Configuration of timespace ****************** */
  ini_set("display_errors", TRUE);
  date_default_timezone_set( "Europe/Prague" );

  /* ***************** Chceking the actuall mode of app ********************** */
    $mode = $app->show("mode");
    switch($mode){
      case '1': //Normal mode
        //Don't do anything spectacular
        break;

      case '2': //Debug mode;
    /*    echo "app class: ";
        var_dump($app); //Show configuration and structure of app class
        echo "app config: ";
        var_dump($app_config);
        echo "default config: ";
        var_dump($default_config);*/
        break;

      default:
        echo '<h3 class="red center"> This mode is invalid </h3>';
    }

  /* **************** Checking the actual lockdown status of app ******************** */
    $lockdown = $app->show("lockdown");
    if($lockdown == true){
      //include_once "resources/user_configuration/lockdown.php";
      echo
      '
        <!DOCTYPE html>
        <html lang="en" dir="ltr">
          <head>
            '.$app->getAdminHTMLHeader('Lockdown').'
          </head>
          <body>
            <div class="row">
              <div class="col s10 offset-s1">
                <h1 class="center"> '.$app->show("websiteTitle").' </h1>
                <h3 class="center"> This website is currently in Lockdown state. Please, try to access this website later. </h3>
              </div>
            </div>
          </body>
        </html>
      ';

      die(); //Prevent the website from loading more contennt
    }

 ?>
