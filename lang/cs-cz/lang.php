<?php

  $conf = array(
    'Name' => 'Čeština',
    'Author' => 'Jan Kočvara',
    'ShortName' => 'cs-cz',
    'LangVer' => '0.1a',
  );

  /*
    Err  = Error message (red alert);
    Succ = Sucesfull message (normal alert);
    Dev = Developer error (alert for developer only);
    Sys = System alerts (alerts as restarting system etc);
  */
  $lang = array(
    //core.php
    'succLoggedOut' => 'Byl jste úspěšně odhlášen', //Succ
    'nonExistingOperation' => 'Pokoušíte se provést neexistující operaci', //Err
    'lowPermission' => 'K provedení této operace nemáte dostatečné oprávnění', //Err
    'operationNeedLogin' => 'K provedení této operace se musíte nejdříve přihlásit', //Err

    //object - app.php
    'nonExistingLogin' => 'Nemohli jsme nalézt účet s tímto heslem', //Err
    'succLogin' => 'Byl jste úspěšně přihlášen, vítejte zpět ', /* Leave a blank gap behind so it will be finished with frst name of user*/ //Succ
    'emptyFields' => 'Prosíme, vyplňte všechny potřebná políčka ', //err
    'succRegister' => 'Váš účet byl úspěšně vytvořen ', //Succ
    'sectionNeedLogin' => 'K přístupu do této sekce se musíte nejdříve přihlásit ', //Err
    'registrationsNotAllowed' => 'Aktuálně není možné se registrovat (Registrace vypnuta) ', //Err
    'alredyExistingEmail_reg' => 'Tento email je již registrován ', //Err
    'existingLevel' => 'Úroveň s tímto názvem již existuje ', //Err
    'succLevelCreated' => 'Úroveň byla úspěšně vytvořena', //Succ
    'unknownSiteFile' => 'Nastala chyba při zjištění oprávnění pro tuto stránku', //Err

    //object - user.php


    //config.php
    'systemRestart' => 'Systém byl právě restartován', //Sys
  );

  $nav = array(
      'dashboard' => 'Přehled',
      'articles' => 'Články',
      'users' => 'Uživatelé',
      'shop' => 'Obchod',
      'levels' => 'Úrovně',
      'settings' => 'Nastavení',
      'settings-s' => array(
            'settings_users' => 'Nastavení uživatelů',
            'settings_groups' => 'Nastavení skupin',
            'settings_app' => 'Nastavení systému',
            'settings_glob' => 'Globální nastavení'
      ),
      'logout' => 'Odhlásit se',
  );

 ?>
