<?php
  //NEVER ALTER THIS FILE !!!!!
  session_start();
  require_once "config.php";

  $GLOBALS["app"] = $app;

  //announce that system was restarted
  if(isset($_COOKIE["system_restart_action"])){
    unset($_SESSION["sys"]);
    $_SESSION["sys"] = $_SESSION["lang"]["systemRestart"];
    setcookie('system_restart_action', '', time() - 3600);
  }


  //Core of functionality
  $post_action = isset( $_POST["action"] ) ? $_POST["action"] : "";
  $post_firstname = isset( $_POST["firstname"] ) ? $_POST["firstname"] : "";
  $post_surname = isset( $_POST["surname"] ) ? $_POST["surname"] : "";
  $post_email = isset( $_POST["email"] ) ? $_POST["email"] : "";
  $post_password = isset( $_POST["password"] ) ? $_POST["password"] : "";
  $post_level = isset( $_POST["level"] ) ? $_POST["level"] : "";
  $post_name = isset( $_POST["name"] ) ? $_POST["name"] : "";
  $post_description = isset( $_POST["description"] ) ? $_POST["description"] : "";

  $get_action = isset( $_GET["action"] ) ? $_GET["action"] : "";

  //Listen to all POST FORM existing permissions
  $perms = $GLOBALS["app"]->getExistingPermissions();
  $post_permissions = array();
  foreach($perms as $perm => $val){
    ${'post_' . $val} = isset( $_POST["$val"] ) ? $_POST["$val"] : "";
    $post_fetched_perms = isset( $_POST["$val"] ) ? $_POST["$val"] : "";
    $post_permissions[$val] = $post_fetched_perms;
  }
  
  if(!empty($post_action)){
    switch($post_action){
      case 'loginUser':
        loginUser($post_email, $post_password);
        break;

      case 'registerUser':
        registerUser($post_firstname, $post_surname, $post_email, $post_password);
        break;

      case 'registerUserAdmin':
        registerUserAdmin($post_firstname, $post_surname, $post_email, $post_level);
        break;

      case 'logoutUser':
        logoutUser();
        break;

      case 'restartSystem':
        restartSystem();
        break;

      case 'createLevel':
        createLevel($post_name, $post_description, $post_permissions);
        break;

      default:
        error($_SESSION["lang"]["nonExistingOperation"]);
        break;
    }
  }else{
    switch($get_action){
      case 'logoutUser':
        logoutUser();
        break;

      default:
        error($_SESSION["lang"]["nonExistingOperation"]);
        break;
    }
  }
  /* Functions */
  function loginUser($email, $pswd){
    $result = $GLOBALS["app"]->loginUser($email, $pswd);
    if($result == true){
      header("Location: admin/index.php");
      die();
    }else{
      header("Location: index.php");
      die();
    }
  }

  function registerUser($fname, $sname, $email, $pswd){
    if(empty($_SESSION["usr"])){
      $result = $GLOBALS["app"]->registerUser($fname, $sname, $email, $pswd);
      if($result == true){
        header("Location: admin/index.php");
        die();
      }else{
        header("Location: pages/registration.php");
        die();
      }
    }else{
      require_once "objects/user.php";
      $user = unserialize($_SESSION["usr"]);
      if($user->show("level") > 4){
        $result = $GLOBALS["app"]->registerUser($fname, $sname, $email, $pswd);
      }else{
        unset($_SESSION["error"]);
        $_SESSION["error"] = $_SESSION["lang"]["lowPermission"];
        header("Location: admin/index.php");
        die();
      }
    }
  }

  function registerUserAdmin($fname, $sname, $email, $level){
    if(empty($_SESSION["usr"])){
      require_once "objects/user.php";
      $user = unserialize($_SESSION["usr"]);
      if($user->show("Level") > 4){
        $result = $GLOBALS["app"]->registerUserAdmin($fname, $sname, $email, $level);
        if($result == true){
          header("Location: admin/pages/users.php");
          die();
        }else{
          header("Location: admin/pages/users.php");
          die();
        }
      }else{
        unset($_SESSION["error"]);
        $_SESSION["error"] = $_SESSION["lang"]["lowPermission"];
        header("Location: index.php");
        die();
      }
    }else{
      unset($_SESSION["error"]);
      $_SESSION["error"] = $_SESSION["lang"]["operationNeedLogin"];
      header("Location: index.php");
      die();
    }
  }

  function createLevel($name, $descr, $perms){
    $result = $GLOBALS["app"]->createNewLevel($name, $descr, $perms);
    if($result){
      header("Location: admin/index.php");
      die();
    }else{
      header("Location: admin/index.php");
      die();
    }
  }

  function logoutUser(){
    unset($_SESSION["succ"]);
    unset($_SESSION["usr"]);
    $_SESSION["succ"] = $_SESSION["lang"]["succLoggedOut"];
    header("Location: index.php");
    die();
  }

  function error($msg){
    unset($_SESSION["error"]);
    $_SESSION["error"] = $msg;
    header("Location: index.php");
    die();
  }

  function restartSystem(){
    setcookie('system_restart_action', '', time() + (86400 * 30), '/');
    session_destroy();
    header("Location: index.php");
    die();
  }

 ?>
