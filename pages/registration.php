<?php
  require_once "../config.php";
 ?>

 <!DOCTYPE html>
 <html lang="en" dir="ltr">
   <head>
     <?php echo $app->getAdminHTMLHeader('Registration'); ?>
   </head>
   <body>
     <div class="row">
       <div class="col s12">
         <div class="row">
           <h2 class="center"> Registration page </h2>
           <form class="col s8 offset-s2" action="../core.php" method="post">
             <div class="row">
               <div class="input-field col s6">
                 <input id="firstname" type="text" name="firstname" class="validate"/>
                 <label for="firstname"> Your firstname </label>
               </div>
               <div class="input-field col s6">
                 <input id="surname" type="text" name="surname" class="validate"/>
                 <label for="surname"> Your surname </label>
               </div>
             </div>
             <div class="row">
               <div class="input-field col s12">
                 <input id="email" type="email" name="email" class="validate"/>
                 <label for="email"> Your valid email </label>
               </div>
             </div>
             <div class="row">
                <div class="input-field col s12">
                  <input id="password" type="password" name="password" class="validate"/>
                  <label for="password"> Your password </label>
                </div>
             </div>
             <div class="row">
               <div class="right">
                 <button class="btn green" type="submit" name="action" value="registerUser"> Register new account </button>
               </div>
             </div>
           </form>
         </div>
       </div>
     </div>
     <?php echo $app->getAdminFooter(); ?>
   </body>
 </html>
