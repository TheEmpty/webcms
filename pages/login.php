<?php
  require_once "../config.php";
  $app->isUserLogged();
 ?>

 <!DOCTYPE html>
 <html lang="en" dir="ltr">
   <head>
     <?php echo $app->getAdminHTMLHeader('Login'); ?>
   </head>
   <body>
     <div class="row">
       <div class="col s12">
         <div class="row">
           <h2 class="center"> Login page </h2>
         </div>
         <form class="col s6 offset-s3" action="../core.php" method="post">
           <div class="row">
             <div class="input-field col s12">
               <input id="email" type="email" class="validate" name="email"/>
               <label for="email"> Type your email </label>
             </div>
           </div>
           <div class="row">
             <div class="input-field col s12">
               <input id="password" type="password" class="validate" name="password"/>
               <label for="password"> Type your password </label>
             </div>
           </div>
           <div class="row">
             <div class="right">
               <button type="submit" class="btn green" value="loginUser" name="action"> Login </button>
             </div>
           </div>
         </form>
         <?php echo $app->getAdminFooter(); ?>
       </div>
     </div>
   </body>
 </html>
