<?php

  class APP
  {
      //Local system configuration
      private $db;
      private $version;
      private $author;
      private $url;
      private $mail_auth; //Default true - NOT WORKING

      //Database sync system configuration
      private $websiteTitle;
      private $mode; //Deafult 1
      private $registrations; //Default true
      private $theme; //Deafult 1
      private $lockdown; //Default false
      private $favicon;
      private $userGroups; //Deafult true
      private $defaultLevel;
      private $timespace;
      private $language; //Deafult cs-cz

      //Arrays
      private $levels; //Existing levels
      private $groups; //Existing groups
      private $langConf; //Actual language configuration

      //MetaData - system statistics
      private $systemStart;
      private $confCreated;

      function __construct($db, $data, $dc){
        $this->db = $db;
        $this->author = $data["Author"];
        $this->url = $data["Url_index"];
        $this->version = $data["Version"];
        $this->systemStart = date('Y-m-d H:i:s'); //Get current timestamp

        //$this->useDefaultConfiguration($dc); //Use default configuration of app
        $this->syncConfiguration($dc); //Sync configuration with database
        //$this->syncLevels(); //Sync levels and level's settings with database
        //$this->syncGroups(); //Sync groups with database
        //$this->loadLanguage(); //Load language
      }

      public function __sleep(){
        return array('version', 'author', 'url', 'mail_auth', 'websiteTitle', 'mode', 'registrations', 'theme', 'lockdown', 'favicon', 'userGroups', 'levels', 'groups', 'systemStart', 'confCreated', 'defaultLevel', 'timespace', 'language');
      }

      public function fetchPDO($db){
        $this->db = $db;
        return true;
      }

      public function show($el){
        return $this->$el;
      }

      public function loginUser($email, $pswd){
        try{
          $pswd = hash("sha256", $pswd);
          $sql = $this->db->prepare("SELECT us.ID, us.Firstname, us.Surname, us.Email, us.LID, us.Nickname, us.Status, um.Birth, um.Verified, um.Profile_picture, um.Security_hash FROM os_users AS us JOIN os_usersmeta AS um ON us.ID = um.UID WHERE us.Email = :email AND us.Password = :pswd LIMIT 1");
          $sql->execute(array(":email" => $email, ":pswd" => $pswd));
          $result = $sql->fetch(PDO::FETCH_ASSOC);
          if(!empty($result)){
            unset($_SESSION["usr"]);
            $this->msg(true, $_SESSION["lang"]["succLogin"].$result["Firstname"]);
            require_once "user.php";
            $user = new USER($this->db, $result);
            $_SESSION["usr"] = serialize($user);
            return true;
          }else{
          $this->msg(false, $_SESSION["lang"]["nonExistingLogin"]);
          //  var_dump($result);
            return false;
          }
        }catch(PDOException $e){
          echo $e->getMessage();
          return false;
        }
      }

      public function registerUser($fname, $sname, $email, $pswd){
        if($this->registration){ //Check if registrations are allowed
          try{
            if(empty($fname) OR empty($sname) OR empty($email) OR empty($pswd)){
              $this->msg(false, $_SESSION["lang"]["emptyFields"]);
              return false;
            }else{

              $checkMail = $this->db->prepare("SELECT Firstname FROM os_users WHERE Email = :email LIMIT 1");
              $checkMail->execute(array(':email' => $email));
              $result = $checkMail->fetchAll();
              if(!empty($result)){
                $this->msg(false, $_SESSION["lang"]["alreadyExistingEmail_reg"]);
                return false;
              }

              $pswd = hash("sha256", $pswd);
              $sql = $this->db->prepare("INSERT INTO os_users (Firstname, Surname, Email, Password, Level) VALUES (:fname, :sname, :mail, :pswd, :lvl)");
              $sql->execute(array(":fname" => $fname, ":sname" => $sname, ":mail" => $email, ":pswd" => $pswd, ":lvl" => $level));

              $uid = $this->db->lastInsertId();
              $reg_ip = $this->getIP();
              $reg_time = date("Y-m-d H:i:s");
              $profile_pic = $this->url."pictures/default_profile.jpg";
              $unhashed_security_hash = $email ."_+_". $reg_time;
              $security_hash = hash("sha256", $unhashed_security_hash);

              $sqlm = $this->db->prepare("INSERT INTO os_usersmeta (UID, Reg_IP, Reg_time, Profile_picture, Security_hash) VALUES (:uid, :reg_ip, :reg_time, :profile_pic, :sec_hash)");
              $sqlm->execute(array(':uid' => $uid, ':reg_ip' => $reg_ip, ':reg_time' => $reg_time, ':profile_pic' => $profile_pic, ':sec_hash' => $security_hash

            ));
              $this->msg(true, $_SESSION["lang"]["succRegister"]);
              return true; //Succesfull registration
            }
          }catch(PDOException $e){
            echo $e->getMessage();
            return false; //PDO Error
          }
        }else{ //Registrations are not allowed
          return $this->msg(false, $_SESSION["lang"]["registrationsNotAllowed"]);
        }
      }

      public function createNewLevel($name, $descr, $p){
        try{
          if(!empty($name) AND !empty($descr) AND !empty($p)){
            var_dump($perms);
            $lvl = $this->db->prepare("SELECT Name FROM os_levels WHERE Name = :name");
            $lvl->execute(array(":name" => $name));
            $lvl_r = $lvl->fetch();
            if(empty($lvl_r)){
              try{
                //Inserting level (general information, NO PERMS!)
                $sql = $this->db->prepare("INSERT INTO os_levels(Name, Description) VALUES (:name, :des)");
                $sql->execute(array(":name" => $name, ':des' => $descr));
                $lid = $this->db->lastInsertId();

                //Inserting custom permissions for level
                $sql_p = $this->db->prepare("INSERT INTO os_levels_permissions(LID, AdvancedAdmin, ListUsers, EditUsers, ListArticles, EditArticles, ListSettings, EditSettings, ListAPI, EditAPI, ListShop, EditShop, ListPlugins, EditPlugins, ListGroups, EditGroups, ListNotes, EditNotes) VALUES (:lid, :aa, :luser, :euser, :lart, :eart, :lsett, :esett, :lAPI, :eAPI, :lshop, :eshop, :lplugins, :eplugins, :lgrp, :egrp, :lnote, :enote)");
                $sql_p->execute(array(":lid" => $lid, ':aa' => $p["AdvancedAdmin"], ':luser' => $p["ListUsers"], ':euser' => $p["EditUsers"], ':lart' => $p["ListArticles"], ':eart' => $p["EditArticles"], ':lsett' => $p["ListSettings"], ':esett' => $p["EditSettings"], ':lAPI' => $p["ListAPI"], ':eAPI' => $p["EditAPI"], ':lshop' => $p["ListShop"], ':eshop' => $p["EditShop"], ':lplugins' => $p["ListPlugins"], ':eplugins' => $p["EditPlugins"], ':lgrp' => $p["ListGroups"], ':egrp' => $p["EditGroups"], ':lnote' => $p["ListNotes"], ':enote' => $p["EditNotes"]));

                //Updating existing levels in local array
                $this->syncLevels();

                //Reporting result
                $this->msg(true, $_SESSION["lang"]["succLevelCreated"]);
                return true;

              }catch(PDOException $e){ //Error in accessing / altering database / table
                $this->msg(false, $e->getMessage());
                return false;
              }
            }else{ //Level already exists (name is in database)
              $this->msg(false, $_SESSION["lang"]["existingLevel"]);
              return false;
            }
          }else{ //Not enough data from form
            $this->msg(false, $_SESSION["lang"]["emptyFields"]);
            return false;
          }
        }catch(PDOException $e){ //Error in accessing database
          $this->msg("dev_error", $e->getMessage());
          return false;
        }
      }

      public function getHTMLHeader($title){
        return
        '
          <!--Import Google Icon Font-->
          <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
          <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
          <!--Let browser know website is optimized for mobile-->
          <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
          <meta charset="utf-8"/>
          <title> '.$title.' </title>
        ';
      }

      public function getAdminHTMLHeader($title){
        return
        '
          <!--Import Google Icon Font-->
          <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
          <link rel="stylesheet" type="text/css" href="'.$this->url.'css/components/footer.css"/>
          <link rel="stylesheet" type="text/css" href="'.$this->url.'css/master.css"/>
          <!--Import materialize.css-->
          <link type="text/css" rel="stylesheet" href="'.$this->url.'css/materialize/css/materialize.min.css"  media="screen,projection"/>
          <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
          <!--Let browser know website is optimized for mobile-->
          <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
          <meta charset="utf-8"/>
          <title> '.$title.' </title>
          <script>
            $(document).ready(function(){
              M.AutoInit();
            });
          </script>
        ';
      }

      public function getAdminFooter(){
        return
        '
          <script type="text/javascript" src="'.$this->url.'css/materialize/js/materialize.min.js"></script>
        ';
      }

      //Function for showing all existing permission in system
      public function getExistingPermissions(){
        $sql = $this->db->prepare('DESCRIBE os_levels_permissions');
        $sql->execute();
        $perms = $sql->fetchAll(PDO::FETCH_COLUMN);
        $perms_array = array();
        $perms_names = array(
          'LID' => 'Level ID',
          'AdvancedAdmin' => '',
        );
        $perms_description = array(
          'LID' => 'ID aktuálně generovaného levelu',
          'AdvancedAdmin' => 'Povolení zobrazit administrátorské rozhraní',
          'ListUsers' => 'Povolení zobrazit si seznam uživatelů',
          'EditUsers' => 'Povolení editovat/vytvářet jednotlivé uživatele',
          'ListArticles' => 'Povolení zobrazit si seznam příspěvků',
          'EditArticles' => 'Povolení editovat/vytvářet příspěvky',
          'ListSettings' => 'Povolení zobrazit si aktuální nastavení systému',
          'EditSettings' => 'Povolení editovat aktuální nastavení systému',
          'ListAPI' => 'Povolení zobrazit si seznam registrovaných přístupů API',
          'EditAPI' => 'Povolení editovat/vytvořit přístupy API',
          'ListShop' => 'Povolení zobrazit si komunitní obchod',
          'EditShop' => 'Povolení instalovat komunitní obsah do systému',
          'ListPlugins' => 'Povolení zobrazit seznam nainstalovaných rozšíření',
          'EditPlugins' => 'Povolení instalovat/odinstalovat rozšíření',
          'ListGroups' => 'Povolení zobrazit seznam uživatelských skupin',
          'EditGroups' => 'Povolení editovat/vytvářet uživatelské skupiny',
          'ListNotes' => 'Povolení zobrazit si vytvořené systémové upozornění',
          'EditNotes' => 'Povolení editovat/vytvářet systémové upozornění',
        );
        foreach($perms as $perm){
          $perms_array['Name'][] = $perm;
          $perms_array['Description'][] = $perms_description[$perm];
        }
        //var_dump($perms_array);
        //return $perms_array;
        return $perms;
      }

      //If user is already logged, redirect him to his administration
      public function isUserLogged(){
        if(empty($_SESSION["usr"])){
          return false;
        }else{
          header("Location: ".$this->url."admin/index.php");
          return true;
        }
      }

      //If user is logged, let him stay. If he is not, kick him to login page with error message
      public function verifyUser(){
        if(empty($_SESSION["usr"])){
          $this->msg(false, $_SESSION["lang"]["sectionNeedLogin"]);
          header("Location: ".$this->url."pages/login.php");
          return false;
        }else{
          $user = unserialize($_SESSION["usr"]);
          $user->fetchPDO($this->db);
          return $user;
        }
      }

      public function getContentByLevel($lid){
        foreach($this->levels as $lvl){
          if($lvl["ID"] == $lid){
            $website_files = explode('/', $_SERVER["REQUEST_URI"]);
            return $lvl;
          /*  switch(end($websites_files)){
              case 'levels':
                return $lvl;
                break;

              //In case of unknown site show error alert
              case 'default':
                $this->msg(false, $_SESSION["lang"]["unknownSiteFile"]);
                return false;
                break;
            }*/
          }
        }
      }

      private function getIP(){
               $ipaddress = '';
           if (getenv('HTTP_CLIENT_IP'))
               $ipaddress = getenv('HTTP_CLIENT_IP');
           else if(getenv('HTTP_X_FORWARDED_FOR'))
               $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
           else if(getenv('HTTP_X_FORWARDED'))
               $ipaddress = getenv('HTTP_X_FORWARDED');
           else if(getenv('HTTP_FORWARDED_FOR'))
               $ipaddress = getenv('HTTP_FORWARDED_FOR');
           else if(getenv('HTTP_FORWARDED'))
              $ipaddress = getenv('HTTP_FORWARDED');
           else if(getenv('REMOTE_ADDR'))
               $ipaddress = getenv('REMOTE_ADDR');
           else
               $ipaddress = 'UNKNOWN';
           return $ipaddress;
      }

      private function syncConfiguration($dc){
        try{
          //Fetching data from database
          $sql_s = $this->db->prepare("SELECT * FROM os_settings_global ORDER BY ConfigurationUploaded DESC LIMIT 1");
          $sql_s->execute();
          $conf = $sql_s->fetchAll(); //Global settings
          if(empty($conf)){
            //if configuration weren't created yet, load default configuration and save it in database
            $dcq = $this->db->prepare("INSERT INTO os_settings_global(ConfigurationCreated, WebsiteTitle, Mode, Registrations, Version, Theme, Lockdown, Favicon, UserGroups, MailAuth, DefaultLevel, Language)VALUES(:cc, :wt, :mode, :reg, :ver, :theme, :lock, :fav, :ug, :ma, :dl, :lang)");
            $dcq->execute(array(':cc' => $dc["confCreated"], ':wt' => $dc["websiteTitle"], ':mode' => $dc["mode"], ':reg' => $dc["registrations"], ':ver' => $dc["version"], ':theme' => $dc["theme"], ':lock' => $dc["lockdown"], ':fav' => $dc["favicon"], ':ug' => $dc["userGroups"], ':ma' => $dc["mail_auth"], ':dl' => $dc["defaultLevel"], ':lang' => $dc["language"]));
            //load default configuration
            $this->useDefaultConfiguration($dc);
          }else{
            //load configuration from database
            $this->confCreated = $conf["0"]["ConfigurationCreated"];
            $this->websiteTitle = $conf["0"]["WebsiteTitle"];
            $this->mode = $conf["0"]["Mode"];
            $this->registrations = $conf["0"]["Registrations"];
            $this->theme = $conf["0"]["Theme"];
            $this->lockdown = $conf["0"]["Lockdown"];
            $this->favicon = $conf["0"]["Favicon"];
            $this->userGroups = $conf["0"]["UserGroups"];
            $this->mail_auth = $conf["0"]["MailAuth"];
            $this->defaultLevel = $conf["0"]["DefaultLevel"];
            $this->language = $conf["0"]["Language"];
            $this->version = $conf["0"]["Version"];
            $this->timespace = $conf["0"]["Timespace"];
          }

          $this->syncLevels(); //Check if levels are created and loaded within database
          $this->syncGroups(); //Check if groups are craeted and loaded within database (depending on user setting - dEFAULT false)
          $this->loadLanguage(); //Load language array

          return true;
        }catch(PDOException $e){
          echo $e->getMessage();
          return false;
        }
      }

      private function syncLevels(){
        try{
          $sql = $this->db->prepare("SELECT * FROM os_levels AS l JOIN os_levels_permissions AS sl ON l.ID = sl.LID");
          $sql->execute();
          $levels = $sql->fetchAll();
          $this->levels = $levels;
          return true;
        }catch(PDOException $e){
          echo $e->getMessage();
          return false;
        }
      }

      private function syncGroups(){
        try{
          $sql = $this->db->prepare("SELECT * FROM os_groups");
          $sql->execute();
          $this->groups = $sql->fetchAll();
          return true;
        }catch(PDOException $e){
          echo $e->getMessage();
          return false;
        }
      }

      private function useDefaultConfiguration($dc){
        try{
          $tables = $this->db->prepare("SHOW TABLES LIKE 'os_settings_levels'");
          $tables->execute();
          if(!$tables){
            die(print_r($this->db->errorInfo(), TRUE));
          }
          if($tables->rowCount()>0){echo 'Table exists';}
          var_dump($tables);

          $this->language = $dc["language"];
          $this->mail_auth = $dc["mail_auth"];
          $this->websiteTitle = $dc["websiteTitle"];
          $this->mode = $dc["mode"];
          $this->registrations = $dc["registrations"];
          $this->theme = $dc["theme"];
          $this->lockdown = $dc["lockdown"];
          $this->favicon = $dc["favicon"];
          $this->userGroups = $dc["userGroups"];
          $this->confCreated = $dc["confCreated"];
          $this->defaultLevel = $dc["defaultLevel"];

          return true;
        }catch(Exception $e){
          echo $e->getMessage();
          return false;
        }
      }

      private function msg($state, $msg){
        if($state == true){
          unset($_SESSION["succ"]);
          $_SESSION["succ"] = $msg;
          return true;
        }elseif($state == false){
          unset($_SESSION["error"]);
          $_SESSION["error"] = $msg;
          return true;
        }else{
          unset($_SESSION["dev_error"]);
          $_SESSION["dev_error"] = $msg;
          return true;
        }
        return false;
      }

      private function loadLanguage(){
        if(!empty($this->language)){
          require_once 'lang/'.$this->language.'/lang.php';
          $this->langConf = $conf; //Load language configuration (metadata)
          $_SESSION["lang"] = $lang; //Load language array
          $_SESSION["lang"]["nav"] = $nav; //Load language navbar
          return true;
        }else{
          $this->msg(false, 'We were unable to load your language!');
          return false;
        }
      }

  }

 ?>
