<?php

  class USER
  {

    private $db;

    private $id;
    private $fname;
    private $sname;
    private $nickname;
    private $email;
    private $level;
    private $verified;
    private $status;

    private $birth;
    private $profile_picture;
    private $security_hash;
    private $groups;

    function __construct($db, $data){
      $this->db = $db;
      $this->id = $data["ID"];
      $this->fname = $data["Firstname"];
      $this->sname = $data["Surname"];
      $this->email = $data["Email"];
      $this->level = $data["LID"];
      $this->verified = $data["Verified"];
      $this->status = $data["Status"];
      $this->birth = $data["Birth"];
      $this->profile_picture = $data["Profile_picture"];
      $this->security_hash = $data["Security_hash"];
      $this->nickname = $data["Nickname"];
    }

    public function __sleep(){
      return array('id', 'fname', 'sname', 'email', 'level', 'verified', 'status', 'birth', 'nickname', 'security_hash', 'profile_picture', 'groups');
    }

    public function fetchPDO($db){
      $this->db = $db;
      return true;
    }

    public function show($el){
      return $this->$el;
    }



  }



 ?>
