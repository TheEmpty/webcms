<?php
  require_once "../../config.php";

  $user = $app->verifyUser();
  $lvl = $app->getContentByLevel($user->show("level"));

 ?>
 <!DOCTYPE html>
 <html lang="en" dir="ltr">
   <head>
     <?php echo $app->getAdminHTMLHeader('Levels'); ?>
   </head>
   <body>
     <?php require_once "../../components.php"; ?>
     <div class="row">
       <div class="col s12">
         <?php require_once "../resources/sidenav.php"; ?>
         <div class="col s10 offset-s1">
           <h2 class="center"> Uživatelské úrovně </h2>
         </div>
         <div class="col s8 offset-s2">
           <a class="btn right" href="new_level.php"> Vytvořit novou úroveň </a> </br> </br>
           <table class="striped">
             <thead>
               <tr>
                 <th> Name </th>
                 <th> Description </th>
                 <th> Prefix </th>
                 <th> Action </th>
               </tr>
             </thead>
             <tbody>
              <?php
              $levels = $app->show("levels");
                foreach($levels as $lvl){
                  echo '
                    <tr>
                      <td> '.$lvl["Name"].' </td>
                      <td> '.$lvl["Description"].' </td>
                      <td> '.$lvl["Prefix"].' </td>
                      <td> </th>
                    </tr>
                  ';
                }
               ?>
             </tbody>
           </table>
         </div>
       </div>
     </div>
     <?php echo $app->getAdminFooter(); ?>
   </body>
 </html>
