<?php
  require_once "../config.php";

  $user = $app->verifyUser();

//  $ui = $app->getContentByLevel();
  if($app->show("mode") === 2){
    echo "user: ";
    var_dump($user);

    echo "<br> app: ";
    var_dump($app);

    echo "<br> dc: ";
    include_once "../resources/os/default_config.php";
    var_dump($default_config);
  }
  echo '<form method="post" action="'.$app->show("url").'core.php"> <button class="btn" type="submit" name="action" value="logoutUser"> Logout </button> </form>';

 ?>
 <!DOCTYPE html>
 <html lang="en" dir="ltr">
   <head>
     <?php echo $app->getAdminHTMLHeader('Dashboard'); ?>
   </head>
   <body>
     <?php
        require_once "../components.php";
      ?>
     <div class="row">
       <div class="col s12">
         <?php require_once "resources/sidenav.php"; ?>
        <h1> Vytvoření nové úrovně </h1>
         <form class="col s8 offset-s2" action="<?php echo $app->show("url"); ?>core.php" method="post">
           <div class="row">
             <div class="input-field col s6">
               <input class="validate" type="text" id="name" name="name" minlength="3"/>
               <label for="name"> Název úrovně</label>
             </div>
           </div>
           <div class="row">
             <div class="input-field col s6">
               <textarea id="description" name="description" rows="8" cols="80" class="materialize-textarea validate" minlength="5" data-length="250"></textarea>
               <label for="description"> Popisek úrovně </label>
             </div>
           </div>
           <div class="row">
             <div class="input-field col s6">
               <input disabled type="text" id="prefix" name="prefix" value=""/>
               <label for="prefix"> Prefix (volitelné) </label>
             </div>
           </div>
           <div class="row">
             <h3> Nastavení oprávnění pro úroveň </h3>
             <!-- Switch -->
             <?php
                $perms = $app->getExistingPermissions();
              //  var_dump($perms);
                $firstElement = true;
                foreach($perms as $perm => $val){
                  if($firstElement){
                    $firstElement = false;
                  }else{
                    echo
                    '<div class="row">
                      <div class="switch">
                      <span class="switch_prefix"> '.$val.' </span>
                        <label>
                          Deaktivovat
                          <input type="checkbox" name="'.$val.'">
                          <span class="lever"></span>
                          Povolit
                        </label>
                      </div>
                    </div>';
                  }
                }
              ?>
            </div>
           <div class="row right">
             <button type="submit" class="btn green" name="action" value="createLevel"> Vytvořit úroveň </button>
             <button type="button" class="btn red"> Zrušit </button>
           </div>
         </form>
         <form class="col s8" action="<?php echo $app->show("url"); ?>core.php" method="post">
           <button type="submit" class="btn red" name="action" value="restartSystem"> Restartovat systém </button>
         </form>
       </div>
     </div>
     <?php echo $app->getAdminFooter(); ?>
   </body>
 </html>
