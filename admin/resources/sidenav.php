<?php
  echo '
  <ul id="slide-out" class="sidenav">
    <li>
      <div class="user-view">
        <div class="background">
          <img src="'.rootUrl.'pictures/back.png">
        </div>
        <a href="#user"><img class="circle" src="'.rootUrl.'pictures/default_profile.jpg"></a>
        <a href="#name"><span class="white-text name">'.$user->show("fname").' '.$user->show("sname").'</span></a>
        <a href="#email"><span class="white-text email">'.$user->show("email").'</span></a>
      </div>
    </li>
    <li><a href="'.rootUrl.'admin">'.$_SESSION["lang"]["nav"]["dashboard"].'<i class="material-icons"> dashboard </i> </a> </li>
    <li><a href="'.rootUrl.'admin/pages/users.php">'.$_SESSION["lang"]["nav"]["users"].' <i class="material-icons"> people </i> </a></li>
    <li><a href="'.rootUrl.'admin/pages/articles.php">'.$_SESSION["lang"]["nav"]["articles"].'<i class="material-icons"> library_books </i></a></li>
    <li><a href="'.rootUrl.'admin/pages/shop.php">'.$_SESSION["lang"]["nav"]["shop"].'<i class="material-icons"> widgets </i></a></li>
    <li><a href="'.rootUrl.'admin/pages/levels.php">'.$_SESSION["lang"]["nav"]["levels"].'<i class="material-icons"> filter_9_plus </i></a></li>
    <li class="no-padding">
      <ul class="collapsible collapsible-accordion">
        <li>
          <a class="collapsible-header">'.$_SESSION["lang"]["nav"]["settings"].'<i class="material-icons"> settings </i><i class="right material-icons">arrow_drop_down</i></a>
          <div class="collapsible-body">
            <ul>
              <li><a href="#!">'.$_SESSION["lang"]["nav"]["settings-s"]["settings_users"].'</a></li>
              <li><a href="#!">'.$_SESSION["lang"]["nav"]["settings-s"]["settings_groups"].'</a></li>
              <li><a href="#!">'.$_SESSION["lang"]["nav"]["settings-s"]["settings_app"].'</a></li>
              <li><a href="#!">'.$_SESSION["lang"]["nav"]["settings-s"]["settings_glob"].'</a></li>
            </ul>
          </div>
        </li>
      </ul>
    </li>
    <li> <div class="divider"> </div> </li>
    <li> <a href="'.rootUrl.'core.php?action=logoutUser"> '.$_SESSION["lang"]["nav"]["logout"].' <i class="material-icons"> power_settings_new </i></a></li>
  </ul>
  <div class="col s12">
    <a href="#" data-target="slide-out" class="sidenav-trigger left"><i class="material-icons large">arrow_right</i></a>
  </div>
  ';
?>
